# Indice / Index
1. [Español](#español)
    1. [Sobre mi](#sobreMi)
    2. [Donde seguirme](#seguirme)
    3. [Algunos de mis repos](#repos)
    4. [Tecnologias que conozco](#tecno)
3. [English](#english)
    1. [About me](#aboutMe)
    2. [Reach me on social media](#follow)
    3. [Some of my repositories](#myRepos)
    4. [Tech I know](#tech)

---
---

# (Español)<a name="español"></a>
## Hola Mundo! Mi nombre es Omar 👋<a name="sobreMi"></a>

Soy un Diseñador y Programador de Videojuegos que ama jugarlos pero aun más hacerlos.
Tambien creo cursos para que otras personas aprendan a hacer Videojuegos.

Tengo experiencia programando en C#, C++, JS/TS y Python pero puedo hacerlo en cualquier lenguaje que este relativamente bien documentado.
Tengo experiencia trabajando con los motores más conocidos: Unreal (con C++ y Blueprints), Unity (con C#) y Godot (con GDScript y C#)
Pero también hice juegos con motores menos conocidos o simplemente con librerías: Pilas (JS), Kontra (JS), Phaser (JS), Cocos2D(C#), PyGame (Python), Scratch

Mi fuerte está en la programación (desde mecánicas, interfaces hasta la inteligencia artificial) y el diseño (desde diseño general hasta diseño de niveles, economía in-game hasta el balance del juego). Pero también tengo experiencia trabajando sobre la UI y algunos apartados artísticos (SFX y VFX sobre todo).
Si está relacionado a los Videojuegos seguramente algo hice de eso, hasta de Community Manager y Tester me he disfrazado.

Nadie sabe todo, de hecho nadie sabe ni el 10% de “todo”. Lo importante es esforzarse por aprender y no tener miedo de decir “esto no lo sé” y buscar ayuda.

---

## 📫 Me pueden seguir en<a name="seguirme"></a>
<a href="https://twitter.com/OmarBazziDP" target="_blank" title="Twitter - @OmarBazziDP">
<img src="https://user-images.githubusercontent.com/22348284/168504092-b4b07aed-5044-4805-8a85-94ca606dd2fa.svg" alt="Twitter-Logo" width="60px" height="60px"/>
</a>

<a href="https://www.linkedin.com/in/omar-bazzi-sf/" target="_blank" title="Linkedin - Omar Bazzi">
<img src="https://user-images.githubusercontent.com/22348284/168504412-4aec7fcf-563e-4b98-ae06-449c43f68754.svg" alt="Linkdn-Logo" width="60px" height="60px"/>
</a>

<a href="https://cleric13.itch.io/" target="_blank" title="itch.io - NiceBug Games">
<img src="https://user-images.githubusercontent.com/22348284/168504541-e9951f8e-11a6-4419-8633-253aa2020328.svg" alt="Itchio-Logo" width="60px" height="60px"/>
</a>

---
## :video_game: Aca pueden ver algunos repos de mis juegos<a name="repos"></a>
- https://gitlab.com/Trece1101/Bullet-Rain
- https://gitlab.com/Trece1101/H00K
- https://gitlab.com/Trece1101/Quizquiz
- https://gitlab.com/Trece1101/AnythingGrowth
- https://gitlab.com/Trece1101/WinterHell
- https://gitlab.com/Trece1101/OdiseaAtlantida
- https://gitlab.com/Trece1101/Saltarina
- https://gitlab.com/Trece1101/BreakAndScore
- https://gitlab.com/Trece1101/GatoCuantico
- https://gitlab.com/Trece1101/RotateThatBlock
- https://gitlab.com/Trece1101/BrainGame
- https://gitlab.com/Trece1101/Meteoritos_Juego
- https://gitlab.com/Trece1101/Asteroids404

---
## :computer: Algunas de las Tecnologías - Lenguajes - Herramientas - Engines que utilizo para hacer juegos<a name="tecno"></a>
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/unrealengine/unrealengine-original-wordmark.svg" alt="Unreal-Logo" width="80px" height="80px"/>&nbsp;&nbsp;&nbsp;&nbsp;<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/unity/unity-original-wordmark.svg" alt="Unity-Logo" width="80px" height="80px"/>&nbsp;&nbsp;&nbsp;&nbsp;<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/godot/godot-original-wordmark.svg" alt="Godot-Logo" width="80px" height="80px"/>

<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/csharp/csharp-original.svg" alt="csharp-Logo" width="80px" height="80px"/>&nbsp;&nbsp;&nbsp;&nbsp;<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/cplusplus/cplusplus-original.svg" lt="cpp-Logo" width="80px" height="80px"/>&nbsp;&nbsp;&nbsp;&nbsp;<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/python/python-original.svg" alt="python-Logo" width="80px" height="80px"/>&nbsp;&nbsp;&nbsp;&nbsp;<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/javascript/javascript-original.svg" alt="js-Logo" width="80px" height="80px"/>

<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/git/git-original-wordmark.svg" alt="git-Logo" width="80px" height="80px"/>&nbsp;&nbsp;&nbsp;&nbsp;<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/github/github-original-wordmark.svg" alt="github-Logo" width="80px" height="80px"/>&nbsp;&nbsp;&nbsp;&nbsp;<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/gitlab/gitlab-original-wordmark.svg" alt="gitlab-Logo" width="80px" height="80px"/>

<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/visualstudio/visualstudio-plain-wordmark.svg" alt="vs-Logo" width="80px" height="80px"/>&nbsp;&nbsp;&nbsp;&nbsp;<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/vscode/vscode-original-wordmark.svg" alt="vscode-Logo" width="80px" height="80px"/>

<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/slack/slack-original-wordmark.svg" alt="slack-Logo" width="80px" height="80px"/>&nbsp;&nbsp;&nbsp;&nbsp;<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/trello/trello-plain-wordmark.svg" alt="trello-Logo" width="80px" height="80px"/>&nbsp;&nbsp;&nbsp;&nbsp;<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/jira/jira-original-wordmark.svg" alt="jira-Logo" width="80px" height="80px"/>

##
##
# (English)<a name="english"></a>

## Hello World! Mi name is Omar 👋<a name="aboutMe"></a>

I am a Video Game Designer and Programmer who loves to play them but even more to make them.
I also make courses, for other people to learn how to make videogames.

I have experience programming in C#, C++, JS/TS and Python but I can code in any language that is relatively "well" documented.

I have experience working with the most popular engines: Unreal (C++ and Blueprints), Unity (C#) and Godot (GDScript and C#)
But I also made games with lesser known engines or just plain libraries: Pilas Engine (JS), Kontra (JS), Phaser (JS), Cocos2D(C#), PyGame (Python), Scratch

My main role is programming (from mechanics, interfaces, to AI) and game-design (from general design to level design, in-game economy to game balance). But I also have experience working on the UI and some artistic aspects (SFX and VFX mostly).
If it is related to Videogames, surely I did something of that, even community manager and tester are some of my "rare" roles.

I firmly believe that no one knows everything, in fact, no one knows even 10% of “everything”. The important is to strive to learn and not be afraid to say “I don't know this” and seek help.

---

## 📫 How to reach me<a name="follow"></a>
<a href="https://twitter.com/OmarBazziDP" target="_blank" title="Twitter - @OmarBazziDP">
<img src="https://user-images.githubusercontent.com/22348284/168504092-b4b07aed-5044-4805-8a85-94ca606dd2fa.svg" alt="Twitter-Logo" width="60px" height="60px"/>
</a>

<a href="https://www.linkedin.com/in/omar-bazzi-sf/" target="_blank" title="Linkedin - Omar Bazzi">
<img src="https://user-images.githubusercontent.com/22348284/168504412-4aec7fcf-563e-4b98-ae06-449c43f68754.svg" alt="Linkdn-Logo" width="60px" height="60px"/>
</a>

<a href="https://cleric13.itch.io/" target="_blank" title="itch.io - NiceBug Games">
<img src="https://user-images.githubusercontent.com/22348284/168504541-e9951f8e-11a6-4419-8633-253aa2020328.svg" alt="Itchio-Logo" width="60px" height="60px"/>
</a>

---
## :video_game: Here you can see some of my video games repositories<a name="myRepos"></a>
- https://gitlab.com/Trece1101/Bullet-Rain
- https://gitlab.com/Trece1101/H00K
- https://gitlab.com/Trece1101/Quizquiz
- https://gitlab.com/Trece1101/AnythingGrowth
- https://gitlab.com/Trece1101/WinterHell
- https://gitlab.com/Trece1101/OdiseaAtlantida
- https://gitlab.com/Trece1101/Saltarina
- https://gitlab.com/Trece1101/BreakAndScore
- https://gitlab.com/Trece1101/GatoCuantico
- https://gitlab.com/Trece1101/RotateThatBlock
- https://gitlab.com/Trece1101/BrainGame
- https://gitlab.com/Trece1101/Meteoritos_Juego
- https://gitlab.com/Trece1101/Asteroids404

## :computer: Some Tech - Programming languages - Tools - Engines I use to make games<a name="tech"></a>
<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/unrealengine/unrealengine-original-wordmark.svg" alt="Unreal-Logo" width="80px" height="80px"/>&nbsp;&nbsp;&nbsp;&nbsp;<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/unity/unity-original-wordmark.svg" alt="Unity-Logo" width="80px" height="80px"/>&nbsp;&nbsp;&nbsp;&nbsp;<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/godot/godot-original-wordmark.svg" alt="Godot-Logo" width="80px" height="80px"/>

<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/csharp/csharp-original.svg" alt="csharp-Logo" width="80px" height="80px"/>&nbsp;&nbsp;&nbsp;&nbsp;<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/cplusplus/cplusplus-original.svg" lt="cpp-Logo" width="80px" height="80px"/>&nbsp;&nbsp;&nbsp;&nbsp;<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/python/python-original.svg" alt="python-Logo" width="80px" height="80px"/>&nbsp;&nbsp;&nbsp;&nbsp;<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/javascript/javascript-original.svg" alt="js-Logo" width="80px" height="80px"/>

<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/git/git-original-wordmark.svg" alt="git-Logo" width="80px" height="80px"/>&nbsp;&nbsp;&nbsp;&nbsp;<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/github/github-original-wordmark.svg" alt="github-Logo" width="80px" height="80px"/>&nbsp;&nbsp;&nbsp;&nbsp;<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/gitlab/gitlab-original-wordmark.svg" alt="gitlab-Logo" width="80px" height="80px"/>

<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/visualstudio/visualstudio-plain-wordmark.svg" alt="vs-Logo" width="80px" height="80px"/>&nbsp;&nbsp;&nbsp;&nbsp;<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/vscode/vscode-original-wordmark.svg" alt="vscode-Logo" width="80px" height="80px"/>

<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/slack/slack-original-wordmark.svg" alt="slack-Logo" width="80px" height="80px"/>&nbsp;&nbsp;&nbsp;&nbsp;<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/trello/trello-plain-wordmark.svg" alt="trello-Logo" width="80px" height="80px"/>&nbsp;&nbsp;&nbsp;&nbsp;<img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/jira/jira-original-wordmark.svg" alt="jira-Logo" width="80px" height="80px"/>
